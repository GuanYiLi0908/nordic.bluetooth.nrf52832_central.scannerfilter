

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "WhiteList.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

struct AEON_TPMS_LinkList *NodeBegin 			 = NULL;
struct AEON_TPMS_LinkList *NodeNow  			 = NULL;
struct AEON_TPMS_LinkList *NodePrevious 	 = NULL;

extern COMMUNICATIONFLAG CommunicFlag;

uint32_t BeaconWhiteList_Set(WhiteListImport_t *p_ListImport)
{
	uint8_t tempDeviceID_Array[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH] = { 0 };
	
	/* Whitelist Import */
	for(uint8_t i = 0; i < IMPORT_WHITELIST_ARRAY_RAW_NUMBER; i++)
	{
		memcpy(tempDeviceID_Array, &p_ListImport[i], MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
		uint16_t tempCheck_Buf = tempDeviceID_Array[0] +
														 tempDeviceID_Array[1] +
														 tempDeviceID_Array[2] +
														 tempDeviceID_Array[3];
		if(tempCheck_Buf != 0)
		{
//			NRF_LOG_RAW_INFO("Import Whitelist Device ID : %02X, %02X, %02X, %02X \r\n", p_ListImport[i].DeviceAddress[0], 
//																																									 p_ListImport[i].DeviceAddress[1],
//																																									 p_ListImport[i].DeviceAddress[2],
//																																									 p_ListImport[i].DeviceAddress[3]);
			
			/* Creat new node */
			NodeNow = (struct AEON_TPMS_LinkList*)malloc(sizeof(struct AEON_TPMS_LinkList));
			if(NodeNow == NULL)
			{
				NRF_LOG_ERROR("Not enough memory to create");
				return NRF_ERROR_NO_MEM;
			}
			
			/* Initialization node */
			memset(NodeNow -> DeviceID, 0, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
			NodeNow -> p_NextNode = NULL;
			
			/* Push into linklist */
			memcpy(NodeNow -> DeviceID, &p_ListImport[i], MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH);
			
			if(NodeBegin == NULL)
				NodeBegin = NodeNow;											/* 如果起始節點為空, 則將當前節點設為起始節點 */			
			else
				NodePrevious -> p_NextNode = NodeNow; 		/* 將前一個節點p_NextNode指向當前節點 */
			NodeNow -> p_NextNode = NULL;								/* 把當前節點的p_NexrNode設為NULL */
			NodePrevious = NodeNow;											/* 把前一個節點設為當前節點 */	
		}
	}
	return NRF_SUCCESS;
}

uint32_t BeaconWhiteList_Matching(MANUFACTURER_DATA_RECEIVE *p_manufacDataRx)
{
	
	if(CommunicFlag.ManufacSpecDataPackagingFinish_Flag)
	{
		CommunicFlag.ManufacSpecDataPackagingFinish_Flag = 0;
		
		NodeNow = NodeBegin;
		for(uint8_t i = 0; i < IMPORT_WHITELIST_ARRAY_RAW_NUMBER; i++)
		{
			if(memcmp(NodeNow -> DeviceID, &p_manufacDataRx -> DeviceID, MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH) == 0)
			{
				NRF_LOG_INFO("White List Match");
				
				NodeNow -> Temperature = p_manufacDataRx -> Temperature;
				NodeNow -> Pressure = p_manufacDataRx -> Temperature;
				NRF_LOG_RAW_INFO("Device ID : %02X %02X %02X %02X \r\n", NodeNow -> DeviceID[0],
																																 NodeNow -> DeviceID[1],
																																 NodeNow -> DeviceID[2],
																																 NodeNow -> DeviceID[3]);
					 
				NRF_LOG_RAW_INFO("Temperature : %02X \r\n", NodeNow -> Temperature);
				NRF_LOG_RAW_INFO("Pressure : %02X \r\n", NodeNow -> Pressure);
				return NRF_SUCCESS;
			}	
			else
				NodeNow = NodeNow -> p_NextNode;
		}
		NRF_LOG_INFO("White List not match");
		return NRF_ERROR_NOT_FOUND;
	}	
}

void BeaconWhiteList_Display(void)
{
	NodeNow = NodeBegin;
	while(NodeNow != NULL)
	{
		NRF_LOG_RAW_INFO("Device ID : %02X, %02X, %02X, %02X \r\n", NodeNow -> DeviceID[0], 
																																NodeNow -> DeviceID[1],
																																NodeNow -> DeviceID[2],
																																NodeNow -> DeviceID[3]);
		NodeNow = NodeNow -> p_NextNode;
	}
}

//void BeaconWhiteList_Release(void)
//{
//	Body = Begin;
//	while(Body != NULL)
//	{
//		End = Body;
//		Body = Body->next;
//		free(End);
//	}
//}
