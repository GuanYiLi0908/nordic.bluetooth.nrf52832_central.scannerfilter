

#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "app_uart.h"
		#include "nrf_sdh_ble.h"
		#include "nrf_ble_scan.h"
		#include "ble_nus_c.h"
		
		
		void TestingFnc(void);
		
		/* UART */
		#define UART_TX_BUF_SIZE          256                                   /**< UART TX buffer size. */
		#define UART_RX_BUF_SIZE          256                                   /**< UART RX buffer size. */

		void UART_Initialization(void);
		static void UARTEvent_handler(app_uart_evt_t * p_event);
		
		/* NUSc */
		#define ECHOBACK_BLE_UART_DATA  1                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */

		void NUSc_Initialization(void);
		
		static void BLE_NUScEvent_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt);
		static void BLE_NUS_UARTRxPrint(uint8_t * p_data, uint16_t data_len);		
		
		/* Bluetooth */
		#define APP_BLE_CONN_CFG_TAG      1                                     /**< Tag that refers to the BLE stack configuration that is set with @ref sd_ble_cfg_set. The default tag is @ref APP_BLE_CONN_CFG_TAG. */
		#define APP_BLE_OBSERVER_PRIO     3                                     /**< BLE observer priority of the application. There is no need to modify this value. */
		
		#define ManufacSpecPayload_NoUseOffset 7																/* Unused Information length of Manufacturer Specific Payload */
		#define MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH 		4								/* Major and Minor data length of Bluetooth definition */
		#define MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH 				4								/* AEON TPMS Device ID length */
	
		typedef struct 
		{
			uint8_t DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Temperature;
			uint8_t Pressure;
		}MANUFACTURER_DATA_RECEIVE;
		
		typedef struct
		{
			uint8_t ManufacSpecDataPackagingFinish_Flag : 1;
		}COMMUNICATIONFLAG;
			
		void BLE_ProtocolStack_Initialization(void);
		void BLE_Scan_Initialization(void);
		void BLE_Discovery_Initialization(void);
		void BLE_Scan_Start(void);
		
		static void DiscoveryEvent_handler(ble_db_discovery_evt_t * p_evt);
		static void ScanEvent_handler(scan_evt_t const * p_scan_evt);
		static void BLEEvent_handler(ble_evt_t const * p_ble_evt, void * p_context);
		
#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_H */
